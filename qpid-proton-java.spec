Name:                qpid-proton-java
Version:             0.12.2
Release:             1
Summary:             Java libraries for Qpid Proton
License:             ASL 2.0
URL:                 http://qpid.apache.org/proton/
Source0:             http://archive.apache.org/dist/qpid/proton/%{version}/qpid-proton-%{version}.tar.gz
BuildRequires:       maven-local mvn(junit:junit) mvn(org.apache:apache:pom:)
BuildRequires:       mvn(org.apache.maven.doxia:doxia-module-markdown)
BuildRequires:       mvn(org.apache.maven.plugins:maven-site-plugin)
BuildRequires:       mvn(org.bouncycastle:bcpkix-jdk15on)
BuildRequires:       mvn(org.apache.geronimo.specs:geronimo-jms_1.1_spec)
BuildRequires:       mvn(org.apache.geronimo.specs:specs:pom:) mvn(org.fusesource.hawtbuf:hawtbuf)
BuildRequires:       mvn(org.fusesource.hawtdispatch:hawtdispatch-transport)
BuildRequires:       mvn(org.mockito:mockito-core)
BuildArch:           noarch
%description
Java language bindings for the Qpid Proton messaging framework.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n qpid-proton-%{version}
rm -r tools docs config.* examples/c examples/javascript \
  examples/php examples/python \
  examples/perl examples/ruby examples/engine/c \
  examples/go examples/cpp CMakeLists.txt bin proton-c \
  tests/smoke tests/ruby tests/python tests/tools tests/javascript
%pom_remove_dep org.python:jython-standalone tests
rm tests/java/org/apache/qpid/proton/JythonTest.java
rm proton-j/src/test/java/org/apache/qpid/proton/engine/impl/FrameParserTest.java
rm contrib/proton-hawtdispatch/src/test/java/org/apache/qpid/proton/hawtdispatch/api/SampleTest.java
%mvn_alias :proton-j org.apache.qpid:proton-api org.apache.qpid:proton-j-impl

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license proton-j/LICENSE
%doc examples/java/messenger/README.txt

%files javadoc -f .mfiles-javadoc
%license proton-j/LICENSE

%changelog
* Thu Aug 20 2020 zhanghua <zhanghua40@huawei.com> - 0.12.2-1
- Package init
